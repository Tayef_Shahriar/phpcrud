<!doctype html>
<html lang="en">
<?php include_once('elements/head.php');?>
<body>
<?php include_once('elements/header.php');?>

<main role="main">
    <div class="container marketing">
    ##MAIN_CONTENT##
    </div>
    <?php
    include_once('elements/footer.php');
    ?>
</main>
<?php include_once('elements/script.php');?>
</body>
</html>