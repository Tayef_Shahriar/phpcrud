<?php
ob_start();
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Product\Product;
//echo $_SESSION['guest_user'];
$product = new Product();
$product = $product->show($_GET['id']);
?>

    <form method="post" action="addtocart.php">
            <div class="row">
                <div class="card mb-3" >
                    <div class="row no-gutters">

                        <div class="col-md-4">
                            <img src="<?=WEBROOT;?>uploads/<?=$product['picture']?>" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <input type="hidden" name="id" value="<?=$product['id']?>" />
                                <h5 class="card-title"><?=$product['title']?></h5>
                                <p class="card-text">BDT.&nbsp;<?=$product['mrp'];?></p>
                                <input type="number" name="qty" value="1" />
                                <button type="submit" class="btn btn-primary">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
<?php
$myContent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $myContent, $userLayout);
?>