<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form method="post" action="order.php" class="form-group">
    <div class="">
        <label for="username">Name</label>
        <input type="text" id="username" name="username" placeholder="Please Enter Your Name.." autofocus="autofocus" class="form-control" required="required"></div>
    <div>
        <label for="mail">Email</label>
        <input id="mail" type="email" required="required" name="email" placeholder="Enter Your Mail.." class="form-control"></div>
    <div>
        <label for="phone">Phone</label>
        <input id="phone" type="text" name="phone" required="required" placeholder="Enter Your Phone No..." class="form-control"></div>
    <div><button type="submit" class="btn btn-primary">Submit</button> </div>
</form>
</body>
</html>
<?php
$myContent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $myContent, $userLayout);
?>