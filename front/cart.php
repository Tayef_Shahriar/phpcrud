<?php
ob_start();
ini_set('display_errors','On');
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");

use kts\Cart\Cart;
$cart = new Cart();
$carts = $cart->all($_SESSION['guest_user']);

//Utility::dd($cart);
?>
        <div class="row">
            <div class="card mb-3" >
                <div class="row no-gutters">
                    <section class="ftco-section ftco-cart">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 ftco-animate">
                                    <div class="cart-list">
                                        <table class="table">
                                            <thead class="thead-primary">
                                            <tr class="text-center">
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $subtotal = 0;
                                            foreach($carts as $cart):
                                                $subtotal = $subtotal + $cart['total_price'];
                                                ?>
                                                <tr class="text-center">
                                                    <td class="product-remove"><a href="#"><span class="ion-ios-close"></span></a></td>

                                                    <td class="image-prod"><div class="img"><img src="<?=WEBROOT;?>uploads/<?php echo $cart['picture']?>" width="140px" height="120px"></div></td>

                                                    <td class="product-name">
                                                        <h3><?php echo $cart['product_title']?></h3>
                                                    </td>

                                                    <td class="price"><?php echo $cart['unite_price']?></td>

                                                    <td class="quantity">
                                                        <form action="updatecart.php" method="post">
                                                            <div class="input-group mb-3">
                                                                <input type="hidden" name="product_id" class="quantity form-control input-number" value="<?php echo $cart['product_id']?>" min="1" max="100">
                                                                <input type="text" name="qty" class="quantity form-control input-number" value="<?php echo $cart['qty']?>" min="1" max="100">
                                                            </div>
                                                            <button type="submit">Update Cart</button>
                                                            <a href="deletecart.php?product_id=<?php echo $cart['product_id']; ?>">Remove From Cart</a>
                                                        </form>

                                                    </td>

                                                    <td class="total"><?php echo $cart['total_price']?></td>
                                                </tr><!-- END TR-->
                                            <?php
                                            endforeach;
                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col col-lg-5 col-md-6 mt-5 cart-wrap ftco-animate">
                                    <div class="cart-total mb-3">
                                        <h3>Cart Totals</h3>
                                        <p class="d-flex">
                                            <span>Subtotal</span>
                                            <span> <?php echo $subtotal;?> BDT</span>
                                        </p>
                                        <p class="d-flex">
                                            <span>Delivery</span>
                                            <span> 0.00BDT</span>
                                        </p>
                                        <p class="d-flex">
                                            <span>Discount</span>
                                            <span> 0.00 BDT</span>
                                        </p>
                                        <hr>
<!--                                        <p class="d-flex total-price">-->
<!--                                            <span>Total</span>-->
<!--                                            <span>--><?php //echo $cart['total_price']?><!--BDT</span>-->
<!--                                        </p>-->
                                    </div>
                                    <p class="text-center"><a href="checkout.php" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p>
                                    <p class="text-center"><a href="http://localhost/practice/front/products.php" class="btn btn-primary py-3 px-4">Go Product List</a></p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
<?php
$myContent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $myContent, $userLayout);
?>
