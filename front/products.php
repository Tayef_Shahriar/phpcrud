<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
ob_start();
use kts\Product\Product;
//echo $_SESSION['guest_user'];
$product = new Product();
$allproducts = $product->all();
?>
        <div class="row">
            <?php
            foreach($allproducts as $product):
                ?>
                <div class="col-lg-4">
                    <div class="card" style="width: 18rem;">
                        <img src="<?=WEBROOT;?>uploads/<?=$product['picture'];?>" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title"><?=$product['title'];?></h5>
                            <p class="card-text">Price: <?=$product['mrp'];?></p>
                            <a href="addToCart.html" class="btn btn-primary">Add To Cart</a>
                            <a href="detail.php?id=<?=$product['id'];?>" class="btn btn-info">View Detail</a>
                        </div>
                    </div>

                </div>
            <?php
            endforeach;
            ?>
        </div>
<?php
    $myContent = ob_get_contents();
    ob_end_clean();
    echo str_replace("##MAIN_CONTENT##", $myContent, $userLayout);
?>