<?php
include_once("vendor/autoload.php");
session_start();
if(array_key_exists('guest_user', $_SESSION) && !empty($_SESSION['guest_user'])){
    //echo $_SESSION['guest_user'];
}else{
    $_SESSION['guest_user'] = 'guest_user'.time();
}
define('WEBROOT', 'http://localhost/practice/');
define('DOCROOT', $_SERVER['DOCUMENT_ROOT'].'/practice/');
$serverName="localhost";
$user="root";
$password="";
$dbName="laracom";
try{
	$conn=new PDO("mysql:host=$serverName; dbname=$dbName", $user, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
	echo "Connection Failed".$e->getMessage();
}
ob_start();
include_once(DOCROOT."admin/layout/layout.php");
$layout = ob_get_contents();
ob_end_clean();
ob_start();
include_once(DOCROOT."front/layout/layout.php");
$userLayout = ob_get_contents();
ob_end_clean();
?>