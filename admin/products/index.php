<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Product\Product;
use kts\Utility\Message;
$product = new Product();
$products = $product->all();

ob_start();
if($message = Message::get()){
    ?>
    <div class="alert alert-success"><?=$message;?></div>
<?php
}
?>
<div>
    <h1>Product</h1>
    <div>
        <button type="button" class="btn btn-sm">
            <a href="<?=WEBROOT;?>admin/products/trash.php">Trash</a> |
            <a href="<?=WEBROOT;?>admin/products/create.php">Add New</a>
        </button>
        <table class="table">
            <thead>
                <th>Picture</th>
                <th>Title</th>
                <th>Price</th>
                <th>Active</th>
                <th>Action</th>
            </thead>
            <tbody>
            <?php
                if($products){
                    foreach($products as $product){
                        ?>
                        <tr>
                            <td><img src="<?=WEBROOT;?>/uploads/<?=$product['picture'];?>" height="100" width="100"></td>
                            <td><a href="show.php?id=<?=$product['id'];?>"><?=$product['title'];?></a></td>
                            <td><?=$product['mrp'];?></td>
                            <td><?php
                                if($product['is_active']){
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                                ?>
                            </td>
                            <td><a href="edit.php?id=<?=$product['id'];?>"><button class="btn btn-outline-primary" >Edit</button></a>  <form action="<?=WEBROOT;?>admin/products/softdelete.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $product['id'];?>">
                                    <button type="submit" onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                <?php
                    }
                }
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php
session_destroy();
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
