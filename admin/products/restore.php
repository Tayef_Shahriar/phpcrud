<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Product\Product;
use kts\Utility\Message;
$id = $_GET['id'];
$product = new Product();
$result = $product->restore($id);
if($result){
    Message::set('Product has been restored successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry...There is a problem. Please try again later.');
    header("location:trash.php");
}
?>