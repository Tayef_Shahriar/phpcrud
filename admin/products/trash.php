<?php
    include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
    use kts\Product\Product;
    $product = new Product();
    $products = $product->trash();
    ob_start();
?>
<div>
    <h1>Trashed Product</h1>
    <div>
        <table class="table">
            <thead>
            <th>Picture</th>
            <th>Title</th>
            <th>Price</th>
            <th>Action</th>
            </thead>
            <tbody>
            <?php
            if($products){
                foreach($products as $product){
                    ?>
                    <tr>
                        <td><img src="<?=WEBROOT;?>/uploads/<?=$product['picture'];?>" height="100" width="100"></td>
                        <td><a href="show.php?id=<?=$product['id'];?>"><?=$product['title'];?></a></td>
                        <td><?=$product['mrp'];?></td>
                        <td><a href="restore.php?id=<?=$product['id'];?>"><button class="btn btn-success" >Restore</button></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
