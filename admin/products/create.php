<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
ob_start();
?>
<form action="store.php" method="post" enctype="multipart/form-data" role="form">
    <div class="form-group">
        <label for="title">Enter Product Title</label>
        <input id="title" type="text" name="title" placeholder="e.g. Oppo Mobile" autofocus="autofocus" class="form-control">
    </div>
    <div class="form-group">
        <label for="picture">Picture</label>
        <input id="picture" type="file" name="picture">
    </div>
    <div class="form-group">
        <label for="mrp">MRP</label>
        <input id="mrp" type="text" name="mrp" class="form-control">
    </div>
    <div class="form-group">
        <label for="active">Active</label>
        <input id="active" type="checkbox" name="is_active" checked="checked" value="1">
    </div>
    <div>
        <button type="submit" class="btn btn-success">Save Product</button>
    </div>
</form>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>