<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Product\Product;
$product = new Product();
$product = $product->show($_GET['id']);
ob_start();
?>

<div>
    <img src="<?=WEBROOT;?>uploads/<?=$product['picture'];?>" class="img-fluid" height="200" width="200">
</div>
<div class="text">
    <?=$product['title'];?>
</div>
<div class="text">
    <?=$product['mrp'];?>
</div>
<div class="text">
    <?php
        if($product['is_active']){
            echo "The Product is active";
        }else{
            echo "The Product is not active";
        }
    ?>
</div>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>