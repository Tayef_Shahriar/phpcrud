<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
use kts\Product\Product;
$product = new Product();
$product = $product->show($_GET['id']);
ob_start();
?>
<form method="post" action="update.php" enctype="multipart/form-data" role="form">
    <div class="text">
        <h1>Edit Product</h1>
    </div>
        <div class="form-group">
            <label for="title">Enter Product Title</label>
            <input id="title" type="text" name="title" value="<?=$product['title'];?>" autofocus="autofocus" class="form-control">
        </div>

            <?php
             if(!empty($product['picture'])){
             ?>
            <img src="<?=WEBROOT;?>uploads/<?=$product['picture'];?>" class="img-fluid" height="150" width="150">
            <?php
             }else{
                 ?>
                 <div>
                     No Image is available. Please upload one.
                 </div>
            <?php
             }
            ?>
        <div class="form-group">
            <label for="picture">Update Picture</label>
            <input id="picture" type="file" name="picture">
        </div>
        <div class="form-group">
            <label for="mrp">MRP</label>
            <input id="mrp" type="text" name="mrp" value="<?=$product['mrp'];?>" class="form-control">
        </div>
        <?php
            $checked = '';
            if($product['is_active']){
                $checked ='checked="checked"';
            }
        ?>
        <div class="form-group">
            <label for="active">Active</label>
            <input id="active" type="checkbox" name="is_active" <?=$checked;?> value="1">
        </div>
        <input type="hidden" name="id" value="<?=$product['id'];?>">
        <input type="hidden" name="prev_picture_name" value="<?=$product['picture'];?>">
        <div>
            <button type="submit" class="btn btn-success">Update Product</button>
        </div>

</form>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
