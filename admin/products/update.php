<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
use kts\Product\Product;
use kts\Utility\Message;
$data = $_POST;
$prev_picture_name = $_POST['prev_picture_name'];
$is_active = 0;
if(array_key_exists('is_active', $_POST)){
    $is_active = $_POST['is_active'];
}
$is_uploaded = false;
if($_FILES['picture']['size']>0){
    $target_file = $_FILES['picture']['tmp_name'];
    $filename = time()."_".str_replace(' ','-', $_FILES['picture']['name']);
    $dest_file = DOCROOT."uploads/".$filename;
    $is_uploaded = move_uploaded_file($target_file, $dest_file);
}
if($is_uploaded){
    $data['picture'] = $filename;
}else{
    $data['picture'] = $prev_picture_name;
}

    $product = new Product();
$result = $product->update($data);
if($result){
    Message::set('Product has been updated successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry...There is a problem. Please try again later.');
    header("location:index.php");
}
?>