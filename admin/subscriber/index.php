<?php
//ob_start();
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
//$layout = ob_get_contents();
//ob_end_clean();
ob_start();
$query="SELECT * FROM subscribers ORDER BY id DESC";
try{
	$sth = $conn->prepare($query);
	$sth->execute();
	$subscribers= $sth->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
?>
<h1>Subscriber</h1>
<div><a href="create.php">Add New</a></div>
<div class="table-responsive">
	<table class="table table-striped table-sm">
		<tr>
			<th>Email</th>
			<th>Action</th>
		</tr>
		<?php
foreach ($subscribers as $subscriber) {
	?>
		<tr>
			<!--<td><img src="<?/*=WEBROOT.'images/'.$brand['picture'];*/?>" height="100" width="100"></td>-->
			<td><div><a href="show.php?id=<?=$subscriber['id'];?>"><?=$subscriber['email'];?></a></div></td>
			<td><a href="edit.php?id=<?=$subscriber['id'];?>">Edit</a> | <a href="delete.php?id=<?=$subscriber['id'];?>">Delete</a></td>
		</tr>
	<?php
}
?>
</table>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>