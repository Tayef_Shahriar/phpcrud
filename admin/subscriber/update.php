<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
$id = $_POST['id'];
$picture = $_POST['picture'];
$title = $_POST['title'];
$uploaded = false;

$isActive = 0;
if(array_key_exists('isActive', $_POST))
{
	$isActive = $_POST['isActive'];
}
$query = "UPDATE `brands` SET 
	`title` = :title, 
	`is_active` = :isActive,
	`modified_at` = :modified_at
	WHERE `brands`.`id` = :id;";
$sth = $conn->prepare($query);
$sth->bindParam(':title', $title);
$sth->bindParam(':isActive', $isActive);
$sth->bindParam(':modified_at', date('Y-m-d h-i-s', time()));
$sth->bindParam(':id', $id);
$result = $sth->execute();
header("location:".WEBROOT."admin/brand/index.php");
?>