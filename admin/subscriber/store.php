<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
$email = $_POST['email'];

$isActive = 0;
if(array_key_exists('isActive', $_POST))
{
	$isActive = $_POST['isActive'];

}
$query = "INSERT INTO `subscriber` ( `email`) VALUES ( :email)";
$sth = $conn->prepare($query);
$sth->bindParam(':email', $email);
$result = $sth->execute();
header("location:".WEBROOT."admin/subscriber/index.php");
?>