<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Product\Categories;
use kts\Utility\Message;
$data = $_POST;

$category = new Categories();
$result = $category->store($data);

if($result){
    Message::set('Product has been added successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry.. There is a problem. Please try again later');
    //log
    header("location:create.php");
}
