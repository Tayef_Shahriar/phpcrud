<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");

use kts\Product\Categories;
use kts\Utility\Message;

$category = new Categories();
$categories = $category->all();

?>
<?php
ob_start();
?>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Category</h1>
        <button type="button" class="btn btn-sm btn-outline-secondary">
            <span data-feather="calendar"></span>
            <a href="create.php" style="color: black">Add New</a>
        </button>

    </div>
    <div class="row">
        <div class="col-md-12 ftco-animate">
            <div class="cart-list">
                <table class="table">
                    <thead class="thead-primary">
                    <tr class="text-center">
                        <th>Name</th>
                        <th>Link</th>

                        <th>Action</th>

                    </tr>
                    </thead>
                    <?php
                    if($categories)
                    {
                    foreach($categories as $category)
                    {
                    ?>
                    <tbody>
                    <tr class="text-center">
                        <td>
                            <h3><a href="show.php"><?php echo  $category['name'];?></a></h3>
                        </td>
                        <td>
                            <h3><?php echo  $category['link'];?></a></h3>
                        </td>

                        <td><a href="edit.php">Edit</a>|
                            <a href="delete.php">Delete</a></td>
                    </tr><!-- END TR-->

                    <?php
                    }}
                    else
                    {
                        ?>
                        <tr><td>There is no more data<a href="create.php"> click here</a> to add data</td></tr>
                    <?php }?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
