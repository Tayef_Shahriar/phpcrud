<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Utility\Message;
?>

<?php
ob_start();
    if($message = Message::get()){
        ?>
        <div class="alert alert-success">
            <?php echo $message;?>
        </div>
        <?php
    }
    ?>

    <form id="contact-form" method="post" action="store.php" role="form">

        <div class="messages"></div>
        <h1>ADD NEW Category</h1>
        <div class="controls">
            <div class="row">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name"
                               value=""
                               type="text"
                               name="name"
                               placeholder="e.g.samsung" class="form-control"
                               autofocus="autofocus";>

                        <div class="help-block text-muted">Enter Category Name</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="link">link</label>
                        <input id="link"
                               value=""
                               type="text"
                               name="link"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>
                </div>

            </div>

        <button type="submit" class="btn btn-success">Save Category</a></button>



    </form>
<?php
session_destroy();
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>

