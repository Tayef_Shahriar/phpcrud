<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/practice/bootstrap.php');
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Create-Sponsor</title>
</head>
<body>
	<form id="contact-form" action="store.php" method="post" enctype="multipart/form-data" role="form">
		<div>
			<label>Title</label>
			<input type="text" name="title" autofocus="autofocus" placeholder="Enter Title">
		</div>
		<div>
			<input type="file" name="picture">
		</div>
		<div>
			<label>Active</label>
			<input type="checkbox" name="isActive" value="1" checked="checked">
		</div>
		<div>
			<button type="Submit">Submit</button>
		</div>
	</form>
</body>
</html>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>