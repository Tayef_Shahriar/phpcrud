<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/practice/bootstrap.php');
$id = $_POST['id'];
$picture = $_POST['picture'];
$title = $_POST['title'];
$uploaded = false;
if($_FILES['picture']['size']>0){
	$tempFile = $_FILES['picture']['tmp_name'];
	$destFile = $_SERVER["DOCUMENT_ROOT"]."/practice/images/".time()."_".str_replace(' ', '-', $_FILES['picture']['name']);
	$uploaded = move_uploaded_file($tempFile, $destFile);
}
if($uploaded){
	$picture = time()."_".str_replace(' ', '-', $_FILES['picture']['name']);
}
else{
	$picture = $picture;
}
$isActive = 0;
if(array_key_exists('isActive', $_POST))
{
	$isActive = $_POST['isActive'];
}
$query = "UPDATE `sponsers` SET 
	`title` = :title, 
	`picture` = :picture,
	`is_active` = :isActive,
	`modified_at` = :modified_at
	WHERE `sponsers`.`id` = :id;";
$sth = $conn->prepare($query);
$sth->bindParam(':title', $title);
$sth->bindParam(':picture', $picture);
$sth->bindParam(':isActive', $isActive);
$sth->bindParam(':modified_at', date('Y-m-d h-i-s', time()));
$sth->bindParam(':id', $id);
$result = $sth->execute();
header("location:".WEBROOT."admin/sponsers/index.php");
?>