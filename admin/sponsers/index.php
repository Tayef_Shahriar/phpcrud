<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/practice/bootstrap.php');
$query="SELECT * FROM sponsers ORDER BY id DESC";
try{
	$sth = $conn->prepare($query);
	$sth->execute();
	$sponsers = $sth->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
?>
<h1>Sponsers</h1>
<div><a href="create.php">Add New</a></div>
<div class="table-responsive">
	<table class="table table-striped table-sm">
		<tr>
			<th>Picture</th>
			<th>Title</th>
			<th>Active</th>
			<th>Created At</th>
			<th>Action</th>
		</tr>
		<?php
foreach ($sponsers as $sponsor) {
	?>
		<tr>
			<td><img src="<?=WEBROOT.'images/'.$sponsor['picture'];?>" height="100" width="100"></td>
			<td><div><a href="show.php?id=<?=$sponsor['id'];?>"><?=$sponsor['title'];?></a></div></td>
			<td><?php 
				if($sponsor['is_active']){
					echo "Yes";
				}else{
					echo "No";
				}
			?>
			</td>
			<td><?= $sponsor['created_at']; ?></td>
			<td><a href="edit.php?id=<?=$sponsor['id'];?>">Edit</a> | <a href="delete.php?id=<?=$sponsor['id'];?>">Delete</a></td>
		</tr>
	<?php
}
?>
</table>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>