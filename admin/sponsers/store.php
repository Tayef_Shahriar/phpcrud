<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/practice/bootstrap.php');
$title = $_POST['title'];
$tempFile = $_FILES['picture']['tmp_name'];
$picName = time()."_".str_replace(' ', '-', $_FILES['picture']['name']);
$destFile = $_SERVER["DOCUMENT_ROOT"]."/practice/images/".$picName;
echo $destFile;
$uploaded = move_uploaded_file($tempFile, $destFile);
if($uploaded){
	$picture = $picName;
}
else{
	$picture = "";
}
$isActive = 0;
if(array_key_exists('isActive', $_POST))
{
	$isActive = $_POST['isActive'];
}
$query = "INSERT INTO `sponsers` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES (NULL, :title, :picture, NULL, NULL, NULL, :isActive, NULL, NULL, :created_at, :modified_at)";
$sth = $conn->prepare($query);
$sth->bindParam(':title', $title);
$sth->bindParam(':picture', $picture);
$sth->bindParam(':isActive', $isActive);
$sth->bindParam(':created_at', date('Y-m-d h-i-s', time()));
$sth->bindParam(':modified_at', date('Y-m-d h-i-s', time()));
$result = $sth->execute();
header("location:".WEBROOT."admin/sponsers/index.php");
?>