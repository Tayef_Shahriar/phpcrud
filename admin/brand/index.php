<?php
ob_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/practice/admin/layout/layout.php');
$layout = ob_get_contents();
ob_end_clean();
ob_start();
$query="SELECT * FROM brands ORDER BY id DESC";
try{
	$sth = $conn->prepare($query);
	$sth->execute();
	$brands= $sth->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
?>
<h1>Brands</h1>
<div><a href="create.php">Add New</a></div>
<div class="table-responsive">
	<table class="table table-striped table-sm">
		<tr>
			<th>Title</th>
			<th>Active</th>
			<th>Created At</th>
			<th>Action</th>
		</tr>
		<?php
foreach ($brands as $brand) {
	?>
		<tr>
			<!--<td><img src="<?/*=WEBROOT.'images/'.$brand['picture'];*/?>" height="100" width="100"></td>-->
			<td><div><a href="show.php?id=<?=$brand['id'];?>"><?=$brand['title'];?></a></div></td>
			<td><?php 
				if($brand['is_active']){
					echo "Yes";
				}else{
					echo "No";
				}
			?>
			</td>
			<td><?= $brand['created_at']; ?></td>
			<td><a href="edit.php?id=<?=$brand['id'];?>">Edit</a> | <a href="delete.php?id=<?=$brand['id'];?>">Delete</a></td>
		</tr>
	<?php
}
?>
</table>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>