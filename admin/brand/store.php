<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/practice/bootstrap.php');
$title = $_POST['title'];

$isActive = 0;
if(array_key_exists('isActive', $_POST))
{
	$isActive = $_POST['isActive'];

}
$query = "INSERT INTO `brands` (`title`, `link`,  `is_active`, `created_at`, `modified_at`) VALUES (:title, '',:isActive, :created_at, :modified_at)";
$sth = $conn->prepare($query);
$sth->bindParam(':title', $title);
$a= date('Y-m-d h-i-s', time());
$b=date('Y-m-d h-i-s', time());
$sth->bindParam(':isActive', $isActive);
$sth->bindParam(':created_at',$a);
$sth->bindParam(':modified_at',$b );
$result = $sth->execute();
header("location:".WEBROOT."admin/brand/index.php");
?>