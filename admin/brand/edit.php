<!DOCTYPE html>
<?php
ob_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/practice/admin/layout/layout.php');
$layout = ob_get_contents();
ob_end_clean();
ob_start();
$id=$_GET['id'];
$query="SELECT * FROM brands WHERE id=:id";
try{
	$sth = $conn->prepare($query);
	$sth->bindParam(':id', $id);
	$sth->execute();
	$sponser = $sth->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
$checked='';
if($sponser['is_active']){
	$checked='checked="checked"';
}
?>
<html>
<head>
	<title>Create-Sponsor</title>
</head>
<body>
	<form action="update.php" method="post" enctype="multipart/form-data">
		<div>
			<label>Title</label>
			<input type="text" name="title" value="<?=$sponser['title'];?>" autofocus="autofocus" placeholder="Enter Title">
		</div>
		<!--<div>
			<img src="<?/*=WEBROOT.'images/'.$sponser['picture'];*/?>" height="200" width="200">
		</div>
		<div>
			<input type="file" name="picture">
		</div>-->
		<div>
			<label>Active</label>
			<input type="checkbox" name="isActive" value="1" <?=$checked; ?>">
		</div>
			<input type="hidden" name="id" value="<?=$sponser['id'];?>">
			<input type="hidden" name="picture" value="<?=$sponser['picture'];?>">
		<div>
			<button type="Submit">Update</button>
		</div>
	</form>
</body>
</html>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>