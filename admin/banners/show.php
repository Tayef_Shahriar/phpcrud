<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Promotion\Banner;
$banner = new Banner();
$banner = $banner->show($_GET['id']);
ob_start();
?>

    <div>
        <img src="<?=WEBROOT;?>uploads/<?=$banner['picture'];?>" class="img-fluid" height="200" width="200">
    </div>
    <div class="text">
        <?=$banner['title'];?>
    </div>
    <div class="text">
        <?=$banner['link'];?>
    </div>
    <div class="text">
        <?php
        if($banner['is_active']){
            echo "The Banner is active";
        }else{
            echo "The Banner is not active";
        }
        ?>
    </div>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>