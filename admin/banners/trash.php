<?php
    include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
    use kts\Promotion\Banner;
    $banner = new Banner();
    $banners = $banner->trash();
    ob_start();
?>
<div>
    <h1>Trashed Banner</h1>
    <div>
        <table class="table">
            <thead>
            <th>Picture</th>
            <th>Title</th>
            <th>Price</th>
            <th>Action</th>
            </thead>
            <tbody>
            <?php
            if($banners){
                foreach($banners as $banner){
                    ?>
                    <tr>
                        <td><img src="<?=WEBROOT;?>/uploads/<?=$banner['picture'];?>" height="100" width="100"></td>
                        <td><a href="show.php?id=<?=$banner['id'];?>"><?=$banner['title'];?></a></td>
                        <td><?=$banner['link'];?></td>
                        <td><a href="restore.php?id=<?=$banner['id'];?>"><button class="btn btn-success" >Restore</button></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
