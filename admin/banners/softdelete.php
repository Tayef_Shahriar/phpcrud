<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Promotion\Banner;
use kts\Utility\Message;
if(strtoupper($_SERVER['REQUEST_METHOD'])=='GET'){
    header('location:index.php');
    exit();
}
$banner = new Banner();
$result = $banner->softdelete($_POST['id']);
if($result){
    Message::set('Banner has been deleted successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry...There is a problem. Please try again later.');
    header("location:index.php");
}