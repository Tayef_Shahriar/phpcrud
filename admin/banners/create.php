<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
ob_start();
?>
    <form action="store.php" method="post" enctype="multipart/form-data" role="form">
        <div class="form-group">
            <label for="title">Enter Banner Title</label>
            <input id="title" type="text" name="title" placeholder="e.g. Oppo Mobile" autofocus="autofocus" class="form-control">
        </div>
        <div class="form-group">
            <label for="picture">Picture</label>
            <input id="picture" type="file" name="picture">
        </div>
        <div class="form-group">
            <label for="link">Link</label>
            <input id="link" type="text" name="link" class="form-control">
        </div>
        <div class="form-group">
            <label for="active">Active</label>
            <input id="active" type="checkbox" name="is_active" checked="checked" value="1">
        </div>
        <div>
            <button type="submit" class="btn btn-success">Save Banner</button>
        </div>
    </form>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>