<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Promotion\Banner;
use kts\Utility\Message;
$id = $_GET['id'];
$banner = new Banner();
$result = $banner->restore($id);
if($result){
    Message::set('Banner has been restored successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry...There is a problem. Please try again later.');
    header("location:trash.php");
}
?>