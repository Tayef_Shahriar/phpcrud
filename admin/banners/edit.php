<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
use kts\Promotion\Banner;
$banner = new Banner();
$banner = $banner->show($_GET['id']);
ob_start();
?>
<form method="post" action="update.php" enctype="multipart/form-data" role="form">
    <div class="text">
        <h1>Edit Banner</h1>
    </div>
        <div class="form-group">
            <label for="title">Enter Banner Title</label>
            <input id="title" type="text" name="title" value="<?=$banner['title'];?>" autofocus="autofocus" class="form-control">
        </div>

            <?php
             if(!empty($banner['picture'])){
             ?>
            <img src="<?=WEBROOT;?>uploads/<?=$banner['picture'];?>" class="img-fluid" height="150" width="150">
            <?php
             }else{
                 ?>
                 <div>
                     No Image is available. Please upload one.
                 </div>
            <?php
             }
            ?>
        <div class="form-group">
            <label for="picture">Update Picture</label>
            <input id="picture" type="file" name="picture">
        </div>
        <div class="form-group">
            <label for="link">LINK</label>
            <input id="link" type="text" name="link" value="<?=$banner['link'];?>" class="form-control">
        </div>
        <?php
            $checked = '';
            if($banner['is_active']){
                $checked ='checked="checked"';
            }
        ?>
        <div class="form-group">
            <label for="active">Active</label>
            <input id="active" type="checkbox" name="is_active" <?=$checked;?> value="1">
        </div>
        <input type="hidden" name="id" value="<?=$banner['id'];?>">
        <input type="hidden" name="prev_picture_name" value="<?=$banner['picture'];?>">
        <div>
            <button type="submit" class="btn btn-success">Update Banner</button>
        </div>

</form>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
