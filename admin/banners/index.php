<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Promotion\Banner;
use kts\Utility\Message;
$banner = new Banner();
$banners = $banner->all();

ob_start();
if($message = Message::get()){
    ?>
    <div class="alert alert-success"><?=$message;?></div>
    <?php
}
?>
<div>
    <h1>Banner</h1>
    <div>
        <button type="button" class="btn btn-sm">
            <a href="<?=WEBROOT;?>admin/banners/trash.php">Trash</a> |
            <a href="<?=WEBROOT;?>admin/banners/create.php">Add New</a>
        </button>
        <table class="table">
            <thead>
            <th>Picture</th>
            <th>Title</th>
            <th>Link</th>
            <th>Active</th>
            <th>Action</th>
            </thead>
            <tbody>
            <?php
            if($banners){
                foreach($banners as $banner){
                    ?>
                    <tr>
                        <td><img src="<?=WEBROOT;?>/uploads/<?=$banner['picture'];?>" height="100" width="100"></td>
                        <td><a href="show.php?id=<?=$banner['id'];?>"><?=$banner['title'];?></a></td>
                        <td><?=$banner['link'];?></td>
                        <td><?php
                            if($banner['is_active']){
                                echo "Yes";
                            }else{
                                echo "No";
                            }
                            ?>
                        </td>
                        <td><a href="edit.php?id=<?=$banner['id'];?>"><button class="btn btn-outline-primary" >Edit</button></a>  <form action="<?=WEBROOT;?>admin/banners/softdelete.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $banner['id'];?>">
                                <button type="submit" onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php
session_destroy();
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
