<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/practice/bootstrap.php");
use kts\Promotion\Banner;
use kts\Utility\Message;
$data = $_POST;
$target_file = $_FILES['picture']['tmp_name'];
$filename = time()."_".str_replace('','-',$_FILES['picture']['name']);
$dest_file = DOCROOT.'uploads/'.$filename;
$is_uploaded = move_uploaded_file($target_file, $dest_file);
if($is_uploaded){
    $data['picture'] = $filename;
}else{
    $data['picture'] = "";
}
$banner = new Banner();
$result = $banner->store($data);
if($result){
    Message::set('Banner has been stored successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry...There is a problem. Please try again later.');
    header("location:index.php");
}
?>