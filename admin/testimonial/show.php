<?php

include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");

ob_start();
$id = $_GET['id'];
$query="SELECT * FROM testimonial WHERE id=:id";
try{
	$sth = $conn->prepare($query);
	$sth->bindParam(':id', $id);
	$sth->execute();
	$testimonial = $sth->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
?>
<div><img src="<?=WEBROOT.'images/'.$testimonial['picture'];?>" height="100" width="100"></div>
<?php
echo $testimonial['name'];
echo '<br/>';
if($testimonial['is_active']){
	echo "The sponser is active";
}else{
	echo "The sponser is not active";
}
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>