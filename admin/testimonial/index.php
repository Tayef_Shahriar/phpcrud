<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");

ob_start();
$query="SELECT * FROM testimonial ORDER BY id DESC";
try{
	$sth = $conn->prepare($query);
	$sth->execute();
	$testimonials = $sth->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
?>
<h1>Testimonials</h1>
<div><a href="create.php">Add New</a></div>
<div class="table-responsive">
	<table class="table table-striped table-sm">
		<tr>
			<th>Picture</th>
			<th>Body</th>
			<th>Name</th>
			<th>Designation</th>
			<th>Active</th>
			<th>Created At</th>
			<th>Action</th>
		</tr>
		<?php
foreach ($testimonials as $testimonial) {
	?>
		<tr>
			<td><img src="<?=WEBROOT.'images/'.$testimonial['picture'];?>" height="100" width="100"></td>
			<td><div><a href="show.php?id=<?=$testimonial['id'];?>"><?=$testimonial['body'];?></a></div>
			</td>
			<td><div><a href="show.php?id=<?=$testimonial['id'];?>"><?=$testimonial['name'];?></a></div>
			</td>
			<td><div><a href="show.php?id=<?=$testimonial['id'];?>"><?=$testimonial['designation'];?></a></div>
			</td>
			<td><?php 
				if($testimonial['is_active']){
					echo "Yes";
				}else{
					echo "No";
				}
			?>
			</td>
			<td><?= $testimonial['created_at']; ?></td>
			<td><a href="edit.php?id=<?=$testimonial['id'];?>">Edit</a> | <a href="delete.php?id=<?=$testimonial['id'];?>">Delete</a></td>
		</tr>
	<?php
}
?>
</table>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>