<!DOCTYPE html>
<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
ob_start();
$id=$_GET['id'];
$query="SELECT * FROM testimonial WHERE id=:id";
try{
	$sth = $conn->prepare($query);
	$sth->bindParam(':id', $id);
	$sth->execute();
	$testimonial = $sth->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	$e->getMessage();
}
$checked='';
if($testimonial['is_active']){
	$checked='checked="checked"';
}
?>
<html>
<head>
	<title>Create-Sponsor</title>
</head>
<body>
	<form id="contact-form" action="update.php" method="post" enctype="multipart/form-data" role="form">
		<div>
			<img src="<?=WEBROOT.'images/'.$testimonial['picture'];?>" height="200" width="200">
		</div>
		<div>
			<input type="file" name="picture">
		</div>
		<div>
			<label>Body</label>
			<input type="text" name="body" value="<?=$testimonial['body'];?>" autofocus="autofocus" placeholder="Enter Title">
		</div>
		<div>
			<label>Name</label>
			<input type="text" name="name" value="<?=$testimonial['name'];?>" autofocus="autofocus" placeholder="Enter Title">
		</div>
		<div>
			<label>Designation</label>
			<input type="text" name="designation" value="<?=$testimonial['designation'];?>" autofocus="autofocus" placeholder="Enter Title">
		</div>
		<div>
			<label>Active</label>
			<input type="checkbox" name="isActive" value="1" checked="checked">
		</div>
			<input type="hidden" name="id" value="<?=$testimonial['id'];?>">
			<input type="hidden" name="picture" value="<?=$testimonial['picture'];?>">
		<div>
			<button type="Submit">Submit</button>
		</div>
	</form>	
</body>
</html>
<?php
$content = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $content, $layout);
?>