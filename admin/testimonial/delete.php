<?php
include_once($_SERVER['DOCUMENT_ROOT']."/practice/bootstrap.php");
$id = $_GET['id'];
$query="DELETE FROM `testimonial` WHERE `testimonial`.`id` = :id";
try{
	$sth = $conn->prepare($query);
	$sth->bindParam(':id', $id);
	$sth->execute();
}
catch(PDOException $e){
	$e->getMessage();
}
header("location:".WEBROOT."admin/testimonial/index.php");
?>