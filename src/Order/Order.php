<?php
/**
 * Created by PhpStorm.
 * User: Tayef
 * Date: 4/25/2019
 * Time: 12:18 PM
 */

namespace kts\Order;
use kts\Db\Db;
use PDO;
class Order
{
    public $id;
    public $sid;
    public $name;
    public $email;
    public $phone;
    function __construct()
    {
        $this->conn = Db::connect();
    }
    function __toString()
    {
        return $this->name;
    }
    public function store($data){
        $this->prepare($data);
        $query = "INSERT INTO myorder (sid, name, email, phone) VALUES (:sid, :name, :email, :phone)";
        $sth = $this->conn->prepare($query);
        $sth->bindparam(':sid', $this->sid);
        $sth->bindparam(':name', $this->name);
        $sth->bindparam(':email', $this->email);
        $sth->bindparam(':phone', $this->phone);
        $sth->execute();
        return true;
    }
    private function prepare($data){
        $this->sid = $data['sid'];
        $this->name = $data['username'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        return;
    }
}