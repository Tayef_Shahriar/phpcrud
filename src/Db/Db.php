<?php
/**
 * Created by PhpStorm.
 * User: Tayef
 * Date: 4/19/2019
 * Time: 11:20 PM
 */

namespace kts\Db;
use PDO;

class Db
{
    public static $servername = "localhost";
    public static $username = "root";
    public static $password = "";
    public static $dbname = "laracom";

    static function connect(){
        $conn = new PDO("mysql:host=".self::$servername.";dbname=".self::$dbname, self::$username, self::$password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }
}