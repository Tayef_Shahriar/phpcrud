<?php
/**
 * Created by PhpStorm.
 * User: Tayef
 * Date: 4/19/2019
 * Time: 11:09 PM
 */

namespace kts\Promotion;
use kts\Db\Db;
use PDO;

class Banner
{
    public $id = null;
    public $title = 'N/A';
    public $picture = null;
    public $link = null;
    public $soft_delete = null;
    public $is_active = null;
    public $created_at = null;
    public $modified_at = null;

    function __construct()
    {
        $this->conn = Db::connect();
    }
    function __toString()
    {
        return $this->title;
    }
    function all(){
        $query = "SELECT * FROM banners WHERE soft_delete = 0 ORDER BY id DESC";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    function store($data){
        $this->prepare($data);
        $query = "INSERT INTO `banners` (`title`, `picture`, `link`, `soft_delete`, `is_active`, `created_at`, `modified_at`) VALUES (:title, :picture, :link, 0, :is_active, :created_at, :modified_at)";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':title', $this->title);
        $sth->bindParam(':picture', $this->picture);
        $sth->bindParam(':link', $this->mrp);
        $sth->bindParam(':is_active',$this->is_active);
        $sth->bindParam(':created_at', $this->created_at);
        $sth->bindParam(':modified_at', $this->modified_at);
        $result = $sth->execute();
        return $result;
    }
    private function prepare($data){
        $this->title = $data['title'];
        $this->picture = $data['picture'];
        $this->mrp = $data['link'];
        $this->is_active = $data['is_active'];
        $this->modified_at = date('Y-m-d h:i:s', time());
        if(array_key_exists('id', $data) && !empty($data['id'])){
            $this->id =$data['id'];
        }
        if(!$this->id){
            $this->created_at = date('Y-m-d h:i:s', time());
        }
    }
    function show($id = null){
        if(empty($id)){
            return;
        }
        $query = "SELECT * FROM banners WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id', $id);
        $sth->execute();
        $banner = $sth->fetch(PDO::FETCH_ASSOC);
        return $banner;
    }
    function update($data){
        if(empty($data['id'])){
            return;
        }
        $this->prepare($data);
        $query = "UPDATE `banners` SET `title` = :title,
`picture` = :picture, `link` = :link, `is_active` = :is_active, `modified_at` = :modified_at WHERE `banners`.`id` = :id";

        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id', $this->id);
        $sth->bindParam(':title', $this->title);
        $sth->bindParam(':picture', $this->picture);
        $sth->bindParam(':link', $this->mrp);
        $sth->bindParam(':is_active', $this->is_active);
        $sth->bindParam(':modified_at', $this->modified_at);
        return $sth->execute();
    }
    function softdelete($id){
        if(empty($id)){
            return;
        }
        $query = "UPDATE banners SET soft_delete = 1 WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id', $id);
        return $sth->execute();
    }
    function trash(){
        $query = "SELECT * FROM banners WHERE soft_delete = 1 ORDER BY id DESC";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    function restore($id){
        if(empty($id)){
            return;
        }
        $query = "UPDATE banners SET soft_delete =0 WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id', $id);
        return $sth->execute();
    }
}
?>