<?php
/**
 * Created by PhpStorm.
 * User: Tayef
 * Date: 4/21/2019
 * Time: 12:08 PM
 */

namespace kts\Product;
use kts\Db\Db;
use PDO;
class Categories
{
    public $id = null;
    public $name;
    public $link;
    function __construct()
    {
        $this->conn = Db::connect();
    }
    public function all(){
        $query = "SELECT * FROM categories";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    function store($data){
        $this->prepare($data);
        $query = "INSERT INTO `categories` (`name`, `link`) VALUES (:name2, :link)";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':name2', $this->name);
        $sth->bindParam(':link', $this->link);
        $result=$sth->execute();
        return $result;
    }
    private function prepare($data){
        $this->name = $data['name'];
        $this->link = $data['link'];
        if(array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
    }
}